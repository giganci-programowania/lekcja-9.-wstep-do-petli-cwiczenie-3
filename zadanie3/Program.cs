﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie3
{
    class Program
    {
        // Napisać program konsolowy, który wyświetla tabliczkę mnożenia od 1*1 do 9*9 w formie kwadratu.
        // Wersja rozszerzona:
        // Odwrócić pętle tak, aby tabliczka mnożenia wyświetlała wartość 81 w lewym górnym rogu, 0 w prawym dolnym rogu.

        static void Main(string[] args)
        {
            for (int i = 0; i <= 9; i++)
            {
                Console.Write(i + "\t");
                // \t to znak tabulacji, aby się dobrze formatowało
                for (int j = 1; j <= 9; j++)
                {
                    if (i > 0)
                    {
                        Console.Write(i * j + "\t");
                    }
                    else
                    {
                        Console.Write(j + "\t");
                    }
                }
                Console.Write("\n");
            }
            Console.Write("\n");
            
            // Wersja odwrócona
            for (int i = 9; i >= 0; --i)
            {
                // \t to znak tabulacji, aby się dobrze formatowało
                for (int j = 9; j > 0; --j)
                {
                    
                    if (i > 0)
                    {
                        Console.Write(i * j + "\t");
                    }
                    else
                    {
                        Console.Write(j + "\t");
                    }
                }
                Console.Write(i + "\t");
                Console.Write("\n");
            }


            Console.ReadLine();
        }
    }
}
